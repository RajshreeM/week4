package com.java.training.wk4hw;

import java.util.Arrays;
import java.util.Scanner;
import java.io.*;


public class HomeworkProblems {
	
	public static void main(String[] args) {
		
		//test for caught speeding
		
		System.out.println(caughtSpeeding(60,false)); //should be 0
		System.out.println(caughtSpeeding(65,false)); // should be 1
		System.out.println(caughtSpeeding(65,true)); // should be 0

		
		//compute examples
		//computeEx();
		
		//all 4 num equal?
		//printAllEqual();
		
		//System.out.println(amountDebt());
		
		
		
		//top 3 heights
		//printThreeHeights();
		
		//find perimeter
		System.out.println(findPerimeter(6,7)); //26
		System.out.println(findPerimeter(20,10)); //60
		System.out.println(findPerimeter(2,9)); //22
		
		
		//are these strings empty?
		System.out.println(isEmpty(""));//true
		System.out.println(isEmpty(" "));//false
		System.out.println(isEmpty("a"));//false
		
		//reverseWords
		System.out.println(reverse("Hello World"));
		System.out.println(reverse("The quick brown fox"));
		System.out.println(reverse("Edabit is really helpful!"));


		
		//voting age?
		//eligible();
		
		//studentgrade
		//studentGrade();
		
		//revenue
		//revenueFromSale();
		
		//detect keypress
		//detectKeyPress();

		//printNatural();
		
		//multiplicationTable();
		
		//isPrime
		//isPrime();
		
		//dowhile
		doWhile();
	}
	
	
	/*
	 * computes result for ticket
	 * value: 0=no ticket, 1=small ticket, 2=big ticket. If speed is 60 or
	 * less, the result is 0. If speed is between 61 and 80 inclusive, the 
	 * result is 1. If speed is 81
	 * or more, the result is 2. 
	 * Unless it is your birthday -- on that day, your speed can be 5
	 * higher in all cases.
	 */
	public static int caughtSpeeding(int mile, boolean birthday) {
		
		int result = 0;
		//if its your birthday then your speed can be 5 higher
		if(birthday) {
			mile-=5;
		}
		
		
		if(mile <=60) {
			result = 0;
		}
		else if (mile > 60 && mile <=80) {
			result =1;
		}
		else {
			result =2;
		}
		
		return result;
		
	}
	
	
	/*
	 * computes specified expressions
	 */

	public static void computeEx() {
		System.out.println("(101+0)/3 ->" + (101+0)/3);
		System.out.println("3.06e-6*10000000.1 ->"+ (double)3.06e-6*10000000.1);
		System.out.println("(true && true)->"+ (true&&true));
		System.out.println("(false && true)->"+ (false&&true));
		System.out.println("(false && false) || (true&& true)->"+ ((false&&false)|| (true&& true)));
		System.out.println("(false || false) && (true&& true)->"+ ((false||false)&& (true&& true)));

		
		
	}
	
	/*
	 * accepts 4 integers form user and prints equal if all four are equal 
	 */
	public static void printAllEqual() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input first number: ");
		int fir = sc.nextInt();
		System.out.println("Input second number: ");
		int sec = sc.nextInt();
		System.out.println("Input third number: ");
		int th = sc.nextInt();
		System.out.println("Input fourth number: ");
		int four = sc.nextInt();
		
		if(fir==sec && th==four && fir == four) {
			
		System.out.println("Numbers are equal!");
		
		}else {System.out.println("Numbers are not equal!");}
		
		
	}
	
	/*
	 * computes debt in n months
	 */
	public static int amountDebt() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input number of months: ");
		int months = sc.nextInt();
		
		int borrow=100000;
		
		double result = months*borrow*.04;
		
		return (int)result+borrow + (1000*(months-1));
		
	}
	
	/*
	 * find top 3 building height in descending order
	 */
	public static void printThreeHeights() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input heights of 8 buildings: ");
		String building = sc.nextLine();
		sc.close();
		
		String[] arr = building.split(" ");
		
         String temp;
		//order from least to greatest
		for(int i = 0; i < 7; i++) {
			
			for(int j = i+1; j < 8;j++) {
				if(Integer.parseInt(arr[j]) < Integer.parseInt(arr[i])) {
					temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
				}
			}
			
			
			
		}
		
		System.out.println("Heights of top 3 buildings: " + arr[7] + " "+ arr[6] + " "+arr[5] );
				
	}
	
	
	/*
	 * finds the permiter
	 */
	public static int findPerimeter(int w, int h) {
		return (w*2) + (h*2);
	}
	
	/*
	 * is empty
	 */
	public static boolean isEmpty(String s) {
		return s.isEmpty();
	}
	
	/*
	 * reverses the string
	 */
	public static String reverse(String s) {
		char[] str = s.toCharArray();
		
		String result = "";
		for(int i=str.length-1;i>=0; i--) {
			result += str[i];
			
		}
		return result;
		
	}
	
	/*
	 * Eligible to vote
	 */
	public static void eligible() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your age: ");
		int age = sc.nextInt();
		sc.close();
		if(age>=18) {
			System.out.println("You are eligible to vote.");
		}
		else {
			System.out.println("You are not eligible to vote.");
		}
	}
	
	/*
	 * Student grade
	 */
	public static void studentGrade() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Quiz score: ");
		int quiz = sc.nextInt();
		System.out.println("Midterm score: ");
		int mid = sc.nextInt();
		System.out.println("Final grade: ");
		int finalg = sc.nextInt();
		sc.close();
		//take avg
		int avg = (quiz+mid+finalg)/3;
		
		if(avg>=90) {
			System.out.println("Your grade is A");
		}
		else if(avg >= 70 && avg <90) {
			System.out.println("Your grade is B");
		}
		else if(avg >= 50 && avg <70) {
			System.out.println("Your grade is C");
		
		}
		else {
			System.out.println("Your grade is an F");
		}
		
	
     }
	
	/*
	 * revenue from sale
	 */
	public static void revenueFromSale() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter unit price: ");
		int unit = sc.nextInt();
		System.out.println("Enter quantity: ");
		int quant = sc.nextInt();
		
		double discount =0.0;
		if(quant<= 120 && quant >= 100) {
			discount = .1;
		}
		else if(quant>120) {
			discount =.15;
		}
		else {discount=0.0;}
		
		double result = (unit*quant)-(discount*unit*quant);
		
		System.out.println("The revenue from sale: "+ result+"$");
		System.out.println("After discount:" + (discount*unit*quant) +"$"+ "(" + discount*100+ "%)");
	}
	
	/*
	 * detect key press 0-9
	 */
	
	public static void detectKeyPress() {
		char key = ' ';
		try{
			key = (char)System.in.read();
			}catch(IOException e){System.out.println("Not allowed");};
			switch (key)
			{
			case '0': System.out.println("You pressed 0."); break;
			case '1': System.out.println("You pressed 1."); break;
			case '2': System.out.println("You pressed 2."); break;
			case '3': System.out.println("You pressed 3."); break;
			case '4': System.out.println("You pressed 4."); break;
			case '5': System.out.println("You pressed 5."); break;
			case '6': System.out.println("You pressed 6."); break;
			case '7': System.out.println("You pressed 7."); break;
			case '8': System.out.println("You pressed 8."); break;
			case '9': System.out.println("You pressed 9."); break;
			default: System.out.println("Not allowed!"); break;
	}
	}
	
	/*
	 * natural number print
	 * 
	 */
	public static void printNatural() {
		for(int i=1;i<11;i++) {
			System.out.print(i+" ");
		}
	}
	
	public static void multiplicationTable() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number you want to see the multiplication table for: ");
		int num = sc.nextInt();
		
		System.out.print("X ");
		
		for(int i=1; i <= 12; i++) {
			System.out.print(i + " ");
			
		}
		System.out.print("\n"+num);
		
		
			for(int j =1; j < 12; j++) {
				System.out.print(" "+ num*j);
			}
		

		
	}
	
	
	/*
	 * checks if the number is prime
	 */
	public static void isPrime() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int num = sc.nextInt();
		boolean prime = true;
		
		sc.close();
		for(int i =2; i <= Math.sqrt(num);i++) {
			if(num%i == 0 && num!= i) {
				System.out.print("Not prime!");
				prime=false;
				
				break;
			}
			
			
		}
		if(prime) {System.out.println("It is prime");}
		
		
		
		
		
	}
	
	/*
	 * do while to add two numbers
	 */
	public static void doWhile() {
		Scanner sc = new Scanner(System.in);
		
		String ch = "";
		
		
		do {
			System.out.print("Enter a number: ");
			int num1 = sc.nextInt();
			System.out.print("Enter a 2nd number: ");
			int num2 = sc.nextInt();
			
			System.out.println("Added: " + (num1+num2));
			
			System.out.print("Would you like to continue? type 'y' to continue: ");
			ch = sc.next();
		}while(ch.equals("y"));
		sc.close();
	}
}



